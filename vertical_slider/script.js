const sliderContainer = document.querySelector('.slider-container')
const sliderRight = document.querySelector('.right-slide')
const sliderLeft = document.querySelector('.left-slide')
const upButton = document.querySelector('.up-button')
const downButon = document.querySelector('.down-button')
const slidersLength = sliderRight.querySelectorAll('div').length

let activeSlideIndex = 0
sliderLeft.style.top = `-${(slidersLength - 1) * 100}vh`

upButton.addEventListener('click', () => changeSlide('up'))
downButon.addEventListener('click', () => changeSlide('down'))

const changeSlide = direction => {
    //获取浏览器高度
    const sliderHeight = sliderContainer.clientHeight
    if (direction === 'up') {
        activeSlideIndex++
        if (activeSlideIndex > slidersLength - 1) {
            activeSlideIndex = 0
        }
    } else if (direction === 'down') {
        activeSlideIndex--
        if (activeSlideIndex < 0) {
            activeSlideIndex = slidersLength - 1
        }
    }

    sliderRight.style.transform = `translateY(-${activeSlideIndex * sliderHeight}px)`
    sliderLeft.style.transform = `translateY(${activeSlideIndex * sliderHeight}px)`
}