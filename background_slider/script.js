const body = document.body
const slide = document.querySelector('.slide')
const leftBtn = document.querySelector('#left')
const rightBtn = document.querySelector('#right')

let activeSlide = 0

setInterval(() => {
    activeSlide++
    if (activeSlide >= 6) {
        activeSlide = 0
    }
    setBgToBodyAndSlider()
}, 3000);

rightBtn.addEventListener('click', () => {
    activeSlide++
    if (activeSlide >= 6) {
        activeSlide = 0
    }
    setBgToBodyAndSlider()
})

leftBtn.addEventListener('click', () => {
    activeSlide--
    if (activeSlide < 0) {
        activeSlide = 5
    }
    setBgToBodyAndSlider()
})

function setBgToBodyAndSlider() {
    body.style.backgroundImage = `url('./img/bg${activeSlide}.jpg')`
    slide.style.backgroundImage = `url('./img/bg${activeSlide}.jpg')`
}