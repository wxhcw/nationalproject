const quizData = [
    {
        question: 'The most commonly used tool in the design phase is the ?',
        a: 'topology chart',
        b: 'flowcharts',
        c: 'object-relationship chart',
        d: 'structure chart',
        correct: 'd'
    },
    {
        question: 'Which refers to the parts of the computer that you can see and touch ?',
        a: 'Hardware',
        b: 'Instruction',
        c: 'Hardship',
        d: 'Software',
        correct: 'a'
    },
    {
        question: 'If we want to retrieve data from the database with SQL, we should use the comand of ?',
        a: 'insert',
        b: 'update',
        c: 'delete',
        d: 'select',
        correct: 'd'
    },
    {
        question: 'Which of the following is not the stages of programming ?',
        a: 'Print the program',
        b: 'Debug the program',
        c: 'Compile the program',
        d: 'Write a program',
        correct: 'a'
    },
]

const quiz = document.getElementById('quiz')
const answerEls = document.querySelectorAll('.answer')
const questionEl = document.getElementById('question')
const a_text = document.getElementById('a_text')
const b_text = document.getElementById('b_text')
const c_text = document.getElementById('c_text')
const d_text = document.getElementById('d_text')
const submitBtn = document.getElementById('submit')

let currentQuiz = 0
let score = 0

loadQuiz()

function loadQuiz() {
    deSelectAnswer()

    const currentQuizData = quizData[currentQuiz]

    questionEl.innerText = currentQuizData.question
    a_text.innerText = currentQuizData.a
    b_text.innerText = currentQuizData.b
    c_text.innerText = currentQuizData.c
    d_text.innerText = currentQuizData.d
}

function deSelectAnswer() {
    answerEls.forEach(answerEl => answerEl.checked = false)
}

function getSelected() {
    const answer = ''
    for (let i = 0; i < answerEls.length; i++) {
        if (answerEls[i].checked) {
            return answerEls[i].id
        }
    }
    return answer
}

submitBtn.addEventListener('click', () => {
    const answer = getSelected()
    if (answer) { //未选择答案
        if (answer === quizData[currentQuiz].correct) {
            score++
        }

        currentQuiz++
        if (currentQuiz < quizData.length) {
            loadQuiz()
        } else if (score === quizData.length) {
            quiz.innerHTML = `
                <h2>Congrations! You have done well!</h2>
                <button onclick="location.reload()">Reload</button>
            `
        } else {
            quiz.innerHTML = `
                <h2>Your answered correctly at ${score}/${quizData.length} questions</h2>
                <button onclick="location.reload()">Reload</button>
            `
        }
    } else {
        console.log('Please select your answer!');
    }
})