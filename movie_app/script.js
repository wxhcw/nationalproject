const API_URL = 'https://api.github.com/search/users?q=""'
const SEARCH_URL = 'https://api.github.com/search/users?q='

const form = document.querySelector('#form')
const search = document.querySelector('#search')
const main = document.querySelector('#main')

getUsers(API_URL)

async function getUsers(url) {
    const res = await fetch(url)
    const { items } = await res.json()
    showUser(items)
}

form.addEventListener('submit', (event) => {
    event.preventDefault()

    const searchTerm = search.value

    if (searchTerm && searchTerm !== '') {
        getUsers(`${SEARCH_URL}${searchTerm}`)
        search.value = ''
    } else {
        window.location.reload()
    }
})

function showUser(users) {
    main.innerHTML = ''
    users.forEach(user => {
        const { avatar_url, login, score, node_id, html_url, id, type } = user
        const userEl = document.createElement('div')
        userEl.classList.add('movie')
        userEl.innerHTML = `
            <img src="${avatar_url}" alt="">
            <div class="movie-info">
                <h3>${login}</h3>
                <span class="${getClassByScore(score)}">${score}</span>
            </div>
            <div class="overview">
                <h3>Overview</h3>
                ${node_id}<br/>
                ${type}Id: ${id}<br/>
                <a href="${html_url}" target="_blank">Contact with ${login}</a>
            </div>
        `
        main.appendChild(userEl)
    });
}

function getClassByScore(score) {
    if (score >= 2) {
        return 'red'
    } else if (score === 1) {
        return 'orange'
    } else {
        return 'green'
    }
}