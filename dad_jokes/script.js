const jokeEl = document.querySelector('#joke')
const jokeBtn = document.querySelector('#jokeBtn')

jokeBtn.addEventListener('click', generateJoke)

async function generateJoke() {
    const config = {
        headers: {
            Accept: 'application/json'
        },
    }

    const res = await fetch('https://icanhazdadjoke.com/', config)
    const {joke} = await res.json()
    jokeEl.innerHTML = joke
}

// generateJoke()

// function generateJoke() {
//     const config = {
//         headers: {
//             Accept: 'application/json'
//         }
//     }
//     fetch('https://icanhazdadjoke.com/', config)
//         .then(res => res.json())
//         .then(({ joke }) => {
//             jokeEl.innerHTML = joke
//         })
// }