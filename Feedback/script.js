const ratings = document.querySelectorAll('.rating')
const ratingsContainer = document.querySelector('.ratings-container')
const sendBtn = document.getElementById('send')
const panel = document.getElementById('panel')
let selectedRating = 'Satisfied'

ratingsContainer.addEventListener('click', e => {
    console.log(e.target,5555);
    if (e.target.parentNode.classList.contains('rating')) {
        removeActive()
        e.target.parentNode.classList.add('active')
        // console.log(e.target.nextElementSibling, 11);
        // console.log(e.target.nextElementSibling.innerHTML, 21);
        selectedRating = e.target.nextElementSibling.innerHTML
    }
})

sendBtn.addEventListener('click', e => {
    panel.innerHTML = `
        <i class='fa fa-heart'></i>
        <strong>Thank You!</strong>
        <br/>
        <strong>Feedback :${selectedRating}!</strong>
        <p>We'll use your feedback to improve our customer support</p>
    `
})

function removeActive() {
    for (let i = 0; i < ratings.length; i++) {
        if (ratings[i].classList.contains('active')) {
            ratings[i].classList.remove('active')
            break;
        }
    }
}