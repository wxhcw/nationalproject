const panels = document.querySelectorAll('.panel')

panels.forEach(panel => {
    panel.addEventListener('click', () => {
        for (const panel of panels) {
            if (panel.classList.value.includes('active')) {
                panel.classList.remove('active')
                break
            }
        }
        panel.classList.add('active')
    })
})
